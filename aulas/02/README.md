# Aula 2: arquivos e buffers 

## Tópicos

- Opções da linha de comando
- Buffers, janelas e abas
- Abrindo e fechando buffers
- Navegação entre buffers
- Netrw (navegação em árvore)
- As metas da segunda semana

## Opções da linha de comando

- Abertura de arquivos:

```
:~$ vim arquivo1 arquivo2 ...
```

- Abertura de arquivos em janelas:

```
:~$ vim -o[n] arquivo1 arquivo2 ... # horizontal
:~$ vim -O[n] arquivo1 arquivo2 ... # vertical
```

- Abertura de arquivos em abas:

```
:~$ vim -p[n] arquivo1 arquivo2 ...
```

- Abertura de diretórios (netrw):

```
:~$ vim caminho/
```

## Buffers, janelas e abas

### Buffer

* O conteúdo de um arquivo na memória.
* Por padrão, buffers precisam ser salvos
  antes de serem ocultados!
* Isso pode ser mudado com:

```
:set hidden
```

#### Dica para o '.vimrc':

```
set hidden
```

### Janelas e abas

- Formas de exibição de buffers.
- Fecha-las não afeta os buffers exibidos.
- O comando `:q` fecha janelas e abas!
- O comando `:qa` fecha todas as janelas e abas!

##  Comandos básicos para operações com buffers

### Abrir, criar e recarregar

- Abrir arquivo em um novo buffer:

```
:e[dit] [arquivo]
:fin[d] <arquivo>
```

- Novo buffer na janela corrente:

```
:enew
```

- Nova janela horizontal com um buffer vazio:

```
:new
```

- Nova janela vertical com um buffer vazio:

```
:vnew
```

- Nova aba com um buffer vazio:

```
:tabnew
```

### Navegação entre buffers

- Listar buffers:

```
:ls
```

- Circulação:

```
:bn          " Próximo buffer
:bp          " Buffer anterior
```

- Navegação:

```
:b N         " Ir para buffer de número N
:b NOME      " Ir para buffer de nome NOME
```

- Alternar entre dois buffers:

```
<C-6>
```

### Salvar e fechar buffers

- Salvar buffers:

```
:w[rite] [arquivo]  " salvar buffer corrente
:wa[ll]             " salvar todos os buffers
:sav[eas] <arquivo> " salvar como...
```

- Descarregar buffer (fechar):

```
:bd[elete]      " Deletar o buffer corrente
:%bd[elete]     " Deletar todos os buffers
:[%]bd!         " Deletar buffers sem salvar
```

> Os buffers descarregados não são excluídos da memória: eles são apenas removidos da lista de buffers.

### Salvar buffer como 'root'

```
:w !sudo tee %
```

- Como funciona:

```
!       " Executa um comando do shell.

:w !    " Faz um pipe do conteúdo do buffer
        " para um comando do shell.

sudo    " Executa comandos como outro usuário.

tee     " Escreve dados recebidos por pipe
        " em stdout e em um arquivo.

%       " Nome do arquivo.
```

### Dica para o '.vimrc':

```
cmap w!! w !sudo tee > /dev/null %
```

## Comandos básicos para janelas e abas

### Abrir arquivos em janelas

```
:sp[lit] [arquivo]      " Nova janela horizontal.
:vsp[lit] [arquivo]     " Nova janela vertical.
```

### Abrir arquivos em abas

```
:tabe[dit] [arquivo]    " Nova aba.
:tabs ball              " Todos os buffers em abas.
```

### Fechar todas as janelas (a visualização)

```
:on[ly]
```

### Fechar todas as abas (a visualização)

```
:tabo[nly]
```

#### Dica para o '.vimrc':

```
:set splitbelow
:set splitright
```

### Movimentação entre janelas

```
<C-w> <direção>     " Ir para janela na direção.
<C-w> w             " Circular entre as janelas.
```

### Movimentação entre abas

```
gt ou <C-PgDown>    " Próxima aba.
gT ou <C-PgUp>      " Aba anterior.
```

## Outras dicas de comandos com janelas e abas


### Abrir arquivo no texto sob o cursor

```
gf          " Abrir buffer para editar o arquivo.
<C-w> f     " Abrir o arquivo em nova janela (hor).
<C-w> v gf  " Abror o arquivo em nova janela (ver).
<C-w> gf    " Abrir o arquivo em nova aba.
```

### Sincronização de rolagem

É possível sincronizar a rolagem de duas janelas verticais contendo textos com as mesmas quantidades de linhas:

```
:set scrollbind     " Executar em ambas as janelas!
```

