# Aula 1: por onde começar...

## Tópicos

- Por que aprender vim/vi
- Um pouco de história
- Editores de linha e visuais
- Criando um script com o 'ed'
- Editando o mesmo script com o 'ex'
- Entendendo o conceito de 'modos'
- As metas da primeira semana
- Comandos e movimentos essenciais

## Por que aprender vim/vi

- O 'vi' é o editor padrão UNIX, \*BSD (nvi) e GNU/Linux.
- Perfeita integração com a plataforma (shell).
- Dominar todo um ecossistema, em vez de um apenas editor.
- Incríveis poderes de edição ("pilhas incluídas").
- Induz bons hábitos de edição naturalmente.
- Altamente customizável (Vim Script + Bash).

##   Um pouco de história...

| Ano  | Quem/O que...                 | Editor   |
|------|-------------------------------|----------|
| 1971 | Ken Thompson                  | 'ed'     |
| 1976 | Bill Joy                      | 'ex'     |
| 1978 | Link para modo visual do 'ex' | 'vi'     |
| 1987 | Tim Thompson                  | 'stevie' |
| 1990 | Steve Kirkendall              | 'elvis'  |
| 1991 | Bram Moolenaar                | 'vim'    |
| 1994 | Keith Bostic                  | 'nvi'    |


## Editores de linha

### ed

- Direcionado a teleimpressoras (tty).
- Modos de comando e de edição.
- Primeira implementação de REGEX.
- Origem do utilitário 'grep'.
- Lê a saída de comandos do shell (!<comando>)\*.
- Salva com 'w' e sai com 'q'.

> (\*) Pelo menos na versão GNU.

### ex

- Feito para terminais em vídeo.
- Introduz o ':' para entrada de comandos.
- Novas sintaxes para substituições com REGEX ('%s').
- Mapeamento de teclas de atalho (':map').
- Abreviações para autotexto (':ab').
- Marcadores de linhas (k<marcador> / '<marcador>).
- Modo de edição visual (':vi').
- Navega entre linhas com 'j' e 'k' no modo visual.

## Editores visuais

### vi

- Começou como um modo do 'ex'.
- Na versão 2.0 do 'ex', era um link simbólico.
- Recebeu atenção dedicada a partir de 1979.
- Teclas de movimentação, comandos e funções.
- Clonado por diversos projetos (elvis, stevie, etc).
- O clone 'stevie' deu origem ao Vim ('VI iMitation').
- O clone 'elvis' deu origem ao 'nvi', padrão nos \*BSD.
- Até hoje, é o editor padrão da família UNIX.
- Mantém os comandos e o modo de edição 'ex'.

### vim

- Desenvolvido a partir do 'stevie' (clone do 'vi').
- Inicialmente, significava 'VI iMitation'.
- Passou a ser 'VI iMproved' em 1993.
- Mantém um modo de compatibilidade com o 'vi'.
- Mantém um modo de edição de linha ('ex').
- Implementa um modo gráfico ('vim-gtk3'/'gvim').
- Implementa a própria linguagem de script.
- Ativamente desenvolvido até hoje.
- Disponível na maioria das distribuições GNU/Linux\*.

> (\*) Instalação opcional.

### Criando um script com editores de linha


Script `salve.sh`

```bash
#!/bin/bash

read -erp 'Digite o seu nome: '
if [[ $REPLY ]]; then
	echo "Salve, $REPLY!"
else
	echo 'Que sem graça... :-('
fi
```

## Entendendo o conceito de 'modos'


### Por que o vim é modal?

- Foi uma solução brilhante para teleimpressoras.

### Por que o vim continua sendo modal?

- Escrever e editar são tarefas diferentes.
- Nós passamos mais tempo editando do que escrevendo.
- Solução brilhante para desenvolver bons hábitos!

## Os quatro principais modos...

```
    ┌────────┐ <a> <A>   ┌────────┐          ┌────────┐
    │ NORMAL │ <i> <I> ▶ │ INSERT │ <esc>  ▶ │ NORMAL │
    └────────┘ <o> <O>   └────────┘          └────────┘

    ┌────────┐   <v>     ┌────────┐          ┌────────┐
    │ NORMAL │   <V>   ▶ │ VISUAL │ <esc>  ▶ │ NORMAL │
    └────────┘  <C-v>    └────────┘          └────────┘

    ┌────────┐           ┌─────────┐ <CR>    ┌────────┐
    │ NORMAL │   <:>   ▶ │ COMMAND │ <esc> ▶ │ NORMAL │
    └────────┘           └─────────┘ <C-c>   └────────┘
```

## As metas da primeira semana

- Utilizar o vi diáriamente (não o vim!).
- Memorizar os principais comandos e movimentos.

### Sobre as teclas de direção:

- Você pode usar as setas, se quiser.
- Mas memorizá-las é fundamental!
- Elas fazem parte de muitos comandos!

### Dicas:

- Foque em se habituar com o vi, não na digitação.
- Nesta semana, apenas anote os "como fazer tal coisa".
- Abrace o desconforto!

## Comandos e movimentos essenciais

### Movimentação do cursor

```
       k                 <Up>
       ↑                  ↑
   h ←   → l     <Left> ←   →  <Right>
       ↓                  ↓
       j                <Down>
```

- Início da linha: `0` ou `<Home>`
- Fim da linha: `$` ou `<End>`
- Página anterior (back): `<C-b>` ou `<PgUp>`
- Próxima página (forward): `<C-f>` ou `<PgDown>`
- Início do buffer: `gg` ou `<C-Home>`
- Fim do buffer: `G` ou `<C-End>`

### Seleção de texto

- Seleção de caracteres: `<v><direções>`
- Seleção de linhas: `<V><direções>` (para cima ou para baixo)
- Seleção em bloco: `<C-v><direções>`

### Recortar (delete) e copiar (yank)

- Copiar seleção: `y`
- Recortar seleção: `d`
- Recortar a linha corrente: `dd`
- Copiar a linha corrente: `yy`
- Recortar várias linhas abaixo: `<#linhas>dd` ou `d<#linhas>j`
- Copiar várias linhas abaixo: `<#linhas>yy` ou `y<#linhas>j`
- Recortar várias linhas acima: `d<#linhas>k`
- Copiar várias linhas acima: `y<#linhas>k`
- Colar (put) depois do cursor: `p`
- Colar (put) antes do cursor: `P`

### Entrar no modo INSERT

- Iniciar inserção depois do cursor: `a`
- Iniciar inserção antes do cursor: `i`
- Iniciar inserção na linha abaixo: `o`
- Iniciar inserção no fim da linha: `A`
- Iniciar inserção no início da linha: `I`
- Iniciar inserção na linha acima: `O`
- Voltar ao modo NORMAL para executar um comando: `<C-o>`
- Voltar ao modo NORMAL: `<esc>`

### Desfazer/refazer

- Desfazer: `U`
- Refazer: `<C-r>`

### Abrir e fechar arquivos

- Abrir arquivo pela linha de comandos:

```
:~$ vi <arquivo> 
```

- Abrir ou criar arquivo:

```
:e <arquivo>    
```

- Fechar arquivo corrente sem salvar:

```
:bd!
```

- Fechar arquivo corrente:

```
:bd             
```

### Salvar e sair

- Salvar:

```
:w [nome]<cr>
```

- Salvar como:

```
:sav <novo_nome><cr>
```

- Salvar e sair:

```
:x [nome]<cr> ou :wq [nome]<cr>
```

- Sair sem salvar:

```
:q!
```

- Sair:

```
:q
```



