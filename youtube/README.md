# Conteúdo apresentado no Youtube

- [Playlist dos vídeos e transmissões ao vivo.](https://www.youtube.com/playlist?list=PLXoSGejyuQGq-pG37oRvExNxvrecjmDeO)
- [Arquivos de configuração atualizados a cada transmissão ao vivo.](.vim)
- [Papel de parede/*cheat sheet* com os principais comandos.](https://codeberg.org/blau_araujo/vim-keys/src/branch/main/wallpapers)

## Lista de alterações

### 6 de novembro de 2023

- Método de dobra alterado para `marker`.
- Caractere de preenchimento de dobras alterado para um espaço.
- Novos atalhos para fechamento de pares no modo inserção.
- Novos atalhos para envolver seleção com pares (valeu, @NRZCode).
- Novos atalhos para rolagem de meia página.
- Alteração do caractere que dispara `<TAB>` em mapeamentos para `<C-Z>`.
- Mapeamento do `Ctrl+S` para inserir *snippets*.
- Criado um diretório com exemplo de *snippets* (`.vim/snippets`).
- Criado um diretório para exemplos de scripts (`.vim/scripts`).
- Novo mapeamento para inserir marcas de dobra com o script em bash `foldmark` (`.vim/scripts/foldmark`).
- Novo design da statusline (baseado no tema *wombat* do plugin [itchyny/lightline.vim](https://github.com/itchyny/lightline.vim)).

---

### 29 de outubro de 2023

- Corrigida a execução de scripts no terminal (`%` para `%:p` -- caminho completo);
- Visualizações deletadas do diretório `~/.vim/view`;
- Comentados os autocomandos para salvar e carregar visualizações;
- Mapeamentos do ins-completion movido para `mappings.vim`.

---

### 28 de outubro de 2023

- Alterada a localização das configurações (`~/.vimrc` para `~/.vim/vimrc`);
- Definidas as váriáveis para o caminho das configurações e o nome do arquivo rc (`vimrc`);
- Separação da configuração em vários arquivos;
- Definições de mapeamentos de teclas (`mapings.vim`);
- Inclusão (*source*) dos arquivos de configuração no `vimrc`;
- Alteradas as cores da barra de status.

